<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/posts', ['as' => 'posts', 'uses' => 'PostController@index']);
Route::post('/post', ['as' => 'post', 'uses' => 'PostController@post']);
Route::post('/post/delete/{id}', ['as' => 'post-delete', 'uses' => 'PostController@delete']);
Route::get('/post/{slug}', ['as' => 'post-view', 'uses' => 'PostController@view']);
Route::delete('/post/{task}', 'PostController@delete');