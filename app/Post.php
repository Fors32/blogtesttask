<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = ['message', 'name', 'slug'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function delete() {
		return parent::delete();
	}

}
