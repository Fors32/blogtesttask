<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
	public function index()
	{
		$posts = Post::orderBy('created_at', 'asc')->get();

		return view('posts.index', [
			'posts' => $posts
		]);
	}

	public function post(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:255',
			'message' => 'required|max:255',
		]);
		$request->user()->posts()->create([
			'message' => $request->message,
			'name' => $request->name,
			'slug' => $request->name,
		]);
		return redirect('/posts');
	}

	public function view($slug){
		$post = Post::where(['slug' => $slug])->first();
		return view('posts.post', [
			'post' => $post
		]);
	}

	public function delete($id) {
		if (\Auth::user()->name == 'admin')
		{
			Post::find($id)->delete();
		}
			return redirect()->to('/posts');
	}

	public function __construct()
	{
		$this->middleware('auth');
	}

}
