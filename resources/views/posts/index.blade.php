<!-- resources/views/tasks/index.blade.php -->

@extends('layouts.app')

@section('content')


    <div class="panel-body">
    @include('common.errors')
<div class="col-md-8 col-md-offset-2">
        @foreach($posts as $post)

        <div class="panel panel-default"><div class="panel-heading">{{$post->user->name}}     <small class="text-center" style="padding-left: 50px; color: grey;">{{$post->created_at}}</small>
                @if(\Auth::user()->name == 'admin')
                <form action="{{route('post-delete', $post->id)}}" method="POST">
                    {{ csrf_field() }}

                    <button type="submit" id="delete-task-{{ $post->id }}" style="float: right;" class="btn btn-danger">
                        <i class="fa fa-btn fa-trash"></i>Удалить
                    </button>
                </form>
                @endif
            </div>

            <div class="panel-body">
                <b><a href="{{route('post-view', $post->slug)}}"> {{$post->name}}: </a></b> {{$post->message}}
            </div></div>
    @endforeach
</div>
        <form action="{{ url('post') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="post-name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="message" class="col-sm-3 control-label">Message</label>

                <div class="col-sm-6">
                    <input type="text" name="message" id="task-name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Добавить сообщение
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection