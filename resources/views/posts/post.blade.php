<!-- resources/views/tasks/index.blade.php -->

@extends('layouts.app')

@section('content')

    {{--Имя автора: {{$post->user->name}}--}}

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                Имя автора: {{$post->user->name}}
            </div>
            <div class="panel-body"><b>Имя: {{$post->name}} : </b>
                <br>
                Сообщение: {{$post->message}}
            </div>
        </div>
    </div>

@endsection